#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


int main() {

    int downloadStatus;
    pid_t downloadPID = fork();

    if (downloadPID < 0) {
        exit(EXIT_FAILURE);
    }

    if (downloadPID == 0) {
        char * cmd_download = "wget \"https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download\" -O hehe.zip NULL";
        system(cmd_download);
    }

    waitpid(downloadPID, & downloadStatus, 0);

    int unzipStat;
    pid_t unzipPid = fork();

    if (unzipPid == 0) {
        char * argv[] = {
            "unzip",
            "-q",
            "hehe.zip",
            NULL
        };
        execv("/usr/bin/unzip", argv);
    }
    waitpid(unzipPid, & unzipStat, 0);

    return 0;
}