#inextensionsCclude <sextensionsCtdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <pthread.h>
#include <time.h>

char extensions[100][100];
int extensionsFolder[100];
int extensionsCount[100];
int totalExtensions = 0;
int maxFiles;

void getCurrentTime(char * timeString) {
    time_t curTime;
    struct tm * timeInfo;

    curTime = time(NULL);

    timeInfo = localtime( & curTime);
    strftime(timeString, 20, "%d-%m-%y %H:%M:%S", timeInfo);
    // printf("%s\n", timeString);

}

void logAccessed(const char * path) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[100];
    logCommand[0] = '\0';
    
    strcat(logCommand, timeString);
    strcat(logCommand, " ACCESSED ");
    strcat(logCommand, path);

    char command[200];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);

}

void logMoved(char * ext, char * source, char * dest) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[200];
    logCommand[0] = '\0';

    strcat(logCommand, timeString);
    strcat(logCommand, " MOVED ");
    strcat(logCommand, ext);
    strcat(logCommand, " file : ");
    strcat(logCommand, source);
    strcat(logCommand, " > ");
    strcat(logCommand, dest);

    char command[100];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);

}

void logMade(char * name) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[100];
    logCommand[0] = '\0';

    strcat(logCommand, timeString);
    strcat(logCommand, " MADE ");
    strcat(logCommand, name);

    char command[100];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);
    
}

void createCategorizedDirectory() {
    system("mkdir categorized");
    logMade("categorized");

    system("cd categorized && mkdir other");
    logMade("other");

    for (int i = 0; i < totalExtensions - 1; i++) {
        char command[255];
        sprintf(command, "mkdir -p categorized/%s", extensions[i]);
        system(command);
        logMade(extensions[i]);
    }
}

void sortExtensions(char arr[][100], int n) {
    char temp[100];
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (strcmp(arr[j], arr[j + 1]) > 0) {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

int isFound(char extension[]) {
    int isFound = 0;
    for (int i = 0; i < totalExtensions; i++) {
        if (strcmp(extension, extensions[i]) == 0) {
            isFound = 1;
        }
    }

    return isFound;
}

void toLowercase(char * str) {
    int i;
    for (i = 0; str[i] != '\0'; i++) {
        str[i] = tolower(str[i]);
    }
}

char * encloseStringWithQuotes(char * str) {
    size_t len = strlen(str);
    char * newStr = malloc(len + 3);
    if (newStr == NULL) {
        fprintf(stderr, "Error: failed to allocate memory\n");
        return NULL;
    }
    newStr[0] = '\'';
    strcpy( & newStr[1], str);
    newStr[len + 1] = '\'';
    newStr[len + 2] = '\0';
    return newStr;
}

int count_files(const char * path) {
    DIR * dir;
    struct dirent * entry;
    int count = 0;

    if ((dir = opendir(path)) == NULL) {
        perror("opendir() error");
        return -1;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry -> d_type == DT_REG) {
            count++;
        }
    }

    closedir(dir);
    return count;
}

int getExtIndex(char * ext) {
    for (int i = 0; i < totalExtensions; i++) {
        if (strcmp(ext, extensions[i]) == 0) {
            return i;
        }
    }

    return -1;
}

void * categorized_thread(void * arg);

void categorized(const char * path) {
    DIR * directory = opendir(path);

    logAccessed(path);

    if (directory == NULL) {
        printf("Could not open directory: %s\n", path);
        return;
    }

    struct dirent * entry;
    while ((entry = readdir(directory)) != NULL) {
        if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
            continue;
        }

        char fullPath[1024];
        snprintf(fullPath, sizeof(fullPath), "%s/%s", path, entry -> d_name);

        if (entry -> d_type == DT_DIR) {
            // printf("Directory: %s\n", fullPath);
            pthread_t tid;
            pthread_create( & tid, NULL, categorized_thread, fullPath);
            pthread_join(tid, NULL);
        } else {
            char * ext = strrchr(entry -> d_name, '.');
            if (ext) {
                ext = ext + 1;
                toLowercase(ext);
            } else ext = "";
            // printf("%s ", fullPath);

            char * newFullPath = encloseStringWithQuotes(fullPath);

            if (isFound(ext)) {
                int extIndex = getExtIndex(ext);
                extensionsCount[extIndex]++;
                // printf("%s %d\n", ext, extIndex);

                if (extensionsFolder[extIndex] == 1) {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

                    int n = count_files(dest);

                    if (n < maxFiles - 1) {
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);
                        logMoved(ext, fullPath, ext);
                    } else {
                        extensionsFolder[extIndex]++;

                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

                        char num[10];
                        sprintf(num, "%d", extensionsFolder[extIndex]);
                        strcat(cmd_mkdir, num);

                        system(cmd_mkdir);
                        // printf("%s\n", cmd_mkdir);
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        logMade(folderName);

                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    }
                } else {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

                    char num[10];
                    sprintf(num, "%d", extensionsFolder[extIndex]);
                    strcat(dest, num);

                    int n = count_files(dest);

                    if (n < maxFiles - 1) {
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    } else {
                        extensionsFolder[extIndex]++;

                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

                        char num[10];
                        sprintf(num, "%d", extensionsFolder[extIndex]);
                        strcat(cmd_mkdir, num);

                        // printf("%s\n", cmd_mkdir);
                        system(cmd_mkdir);
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        logMade(folderName);

                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);
                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    }
                }
            } else {
                extensionsCount[totalExtensions - 1]++;
                char command[100] = "cp ";
                strcat(command, newFullPath);
                strcat(command, " \"./categorized/other\"");

                // printf("%s\n", command);
                system(command);
                logMoved(ext, fullPath, "other");
            }
        }
    }

    closedir(directory);
}

void * categorized_thread(void * arg) {
    const char * path = (const char * ) arg;
    categorized(path);
    pthread_exit(NULL);
}

int main() {

    ///   Initialize Array   ///

    for (int i = 0; i < 100; i++) {
        extensionsFolder[i] = 1;
        extensionsCount[i] = 0;
    }

    ///  Get Extensions from extensions.txt ///

    FILE * fileExtensions = fopen("extensions.txt", "r");

    char buffer[100];

    while (fgets(buffer, 100, fileExtensions) != NULL) {
        int len = strcspn(buffer, "\r\n"); // find position of newline or carriage return character
        buffer[len] = '\0';
        strcpy(extensions[totalExtensions], buffer);
        totalExtensions++;
    }

    fclose(fileExtensions);

    /// Sort Extension in Ascending order    ///

    sortExtensions(extensions, totalExtensions);

    /// Add other extensions    ///

    strcpy(extensions[totalExtensions], "other");
    totalExtensions++;

    /// Get max files number of files from max.txt ///

    FILE * fileMax = fopen("max.txt", "r");

    fgets(buffer, 100, fileMax);
    maxFiles = atoi(buffer);

    fclose(fileMax);

    ///   Create Categorized Dircetory   ///

    createCategorizedDirectory();

    ///   Categorize files   ///

    pthread_t tid;
    pthread_create( & tid, NULL, categorized_thread, "./files");
    pthread_join(tid, NULL);

    for (int i = 0; i < totalExtensions; i++) {
        printf("%s: %d\n", extensions[i], extensionsCount[i]);
    }

    return 0;
}