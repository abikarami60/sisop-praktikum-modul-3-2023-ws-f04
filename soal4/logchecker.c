#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char folders[100][500];
int fileCount[100];
int folderCount = 0;

char extensions[100][500];
int extensionCount[100];
int totalExtension = 0;

void sort(char arr[][500], int n) {
    int i, j;
    char temp[100];
    for (i = 0; i < n - 1; i++) {
        for (j = 0; j < n - i - 1; j++) {
            if (strcmp(arr[j], arr[j + 1]) > 0) {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

int getFolderIndex(char * folder) {
    for (int i = 0; i < folderCount; i++) {
        if (strcmp(folder, folders[i]) == 0) {
            return i;
        }
    }

    return -1;
}

int getExtensionIndex(char * extension) {
    for (int i = 0; i < totalExtension; i++) {
        if (strcmp(extension, extensions[i]) == 0) {
            return i;
        }

    }
    return -1;

}

int main() {
    ///   Insisasi   ///
    for (int i = 0; i < 100; i++) {
        fileCount[i] = 0;
    }

    for (int i = 0; i < 100; i++) {
        extensionCount[i] = 0;
    }

    ///   Buka File   ///	

    FILE * fp = fopen("log.txt", "r");

    if (fp == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }
    char buffer[200];

    ///   Hitung Jumlah ACCESSED   ///
    int AccessedCount = 0;
    while (fgets(buffer, 200, fp)) {
        if (strstr(buffer, "ACCESSED") != NULL) {
            AccessedCount++;
        }
    }

    printf("Jumlah 'ACCESSED': %d\n", AccessedCount);

    ///   List Folder   ///

    fp = fopen("log.txt", "r");
    char * folder;

    while (fgets(buffer, 200, fp)) {
        folder = strstr(buffer, "MADE ");

        if (folder != NULL) {
            folder += strlen("MADE ");
            folder[strlen(folder) - 2] = '\0';
            strcpy(folders[folderCount], folder);
            folderCount++;

        }
    }

    ///   Sort Folder   ///

    sort(folders, folderCount);

    ///   Hitung Jumlah File Tiap Folder   ///

    fp = fopen("log.txt", "r");
    char temp[100];

    while (fgets(buffer, 200, fp)) {
        if (folder = strstr(buffer, "> ")) {
            folder[strlen(folder) - 2] = '\0';
            strcpy(temp, folder + 2);

            int folderIndex = getFolderIndex(temp);

            fileCount[getFolderIndex(temp)]++;

        }

    }

    ///   List Extension   ///

    char * extension;
    fp = fopen("log.txt", "r");
    while (fgets(buffer, 200, fp)) {
        if (extension = strstr(buffer, "MOVED ")) {
            strcpy(temp, extension + 6);
            strcpy(extension, temp);

            if (extension[0] == ' ') {
                strcpy(extension, " ");
            } else {
                char * temp2 = strtok(extension, " ");
                strcpy(extension, temp2);

            }

            int extensionExist = 0;
            for (int i = 0; i < totalExtension; i++) {
                if (strcmp(extension, extensions[i]) == 0) {
                    extensionExist = 1;
                }
            }

            if (extensionExist == 0) {

                strcpy(extensions[totalExtension], extension);
                totalExtension++;

            }

        }
    }

    ///   Sort Extension   ///

    sort(extensions, totalExtension);

    ///   Hitung Jumlah File Tiap Extension   ///

    fp = fopen("log.txt", "r");
    while ((fgets(buffer, 200, fp))) {
        if (extension = strstr(buffer, "MOVED ")) {
            strcpy(temp, extension + 6);
            strcpy(extension, temp);

            if (extension[0] == ' ') {
                strcpy(extension, " ");
            } else {
                char * temp2 = strtok(extension, " ");
                strcpy(extension, temp2);

            }

            int extensionIndex = getExtensionIndex(extension);

            extensionCount[extensionIndex]++;

        }
    }

    ///   Print folder dan extension beserta jumlahnya  ///

    printf("---   list folder:   ---\n");

    for (int i = 1; i < folderCount; i++) {

        printf("%s: %d\n", folders[i], fileCount[i]);
    }

    printf("---   list extension:   ---\n");

    for (int i = 0; i < totalExtension; i++) {

        printf("%s: %d\n", extensions[i], extensionCount[i]);
    }

    fclose(fp);
    return 0;
}