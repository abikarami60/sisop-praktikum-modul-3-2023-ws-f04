#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

#define MSG_SIZE 512 // maximum length of the message that can be sent allowed

char method[1000][100];
char song[1000][1000];

void extractContent(int n) {
    for (int i = 0; i < n; i++) {
        char * methodSubstr = strstr(method[i], ": ");
        if (methodSubstr != NULL) {
            methodSubstr += 2;
        }

        char * commaPtr = strchr(methodSubstr, ',');
        if (commaPtr != NULL) {
            strcpy(commaPtr, commaPtr + 1);
        }

        char * quotePtr = strchr(methodSubstr, '\"');
        while (quotePtr != NULL) {
            strcpy(quotePtr, quotePtr + 1);
            quotePtr = strchr(methodSubstr, '\"');
        }

        // printf("%d %s ", i, methodSubstr);
        strcpy(method[i], methodSubstr);

        char * songSubstr = strstr(song[i], ": ");
        if (songSubstr != NULL) {
            songSubstr += 2;
        }

        int len = strlen(songSubstr);
        for (int i = 0; i < len; i++) {
            songSubstr[i] = songSubstr[i + 1];
        }

        songSubstr[len - 4] = '\0';

        snprintf(song[i], 500, "%s", songSubstr);
        //strcpy(song[i],
    }

}

void rot13(char * song) {
    char * p = song;
    while ( * p != '\0') {
        if ( * p >= 'a' && * p <= 'z') {
            * p = ( * p - 'a' + 13) % 26 + 'a';
        } else if ( * p >= 'A' && * p <= 'Z') {
            * p = ( * p - 'A' + 13) % 26 + 'A';
        }
        p++;
    }

    // printf("%s\n", song);
    char command[100] = "echo \"";
    strcat(command, song);
    strcat(command, "\" >> playlist.txt");
    // printf("%s\n", command);
    system(command);

}

int getBase64Index(char c) {
    if (c >= 'A' && c <= 'Z')
        return c - 'A';
    if (c >= 'a' && c <= 'z')
        return c - 'a' + 26;
    if (c >= '0' && c <= '9')
        return c - '0' + 52;
    if (c == '+')
        return 62;
    if (c == '/')
        return 63;
    return -1;
}

void base64_decrypt(const char * song) {
    size_t songLength = strlen(song);
    size_t decryptedSongLength = (songLength / 4) * 3;
    if (song[songLength - 1] == '=')
        decryptedSongLength--;
    if (song[songLength - 2] == '=')
        decryptedSongLength--;

    char * decryptedSong = (char * ) malloc(decryptedSongLength + 1);
    if (decryptedSong == NULL) {
        printf("Failled to ALlocate Memory\n");
        return;
    }

    size_t i, j;
    for (i = 0, j = 0; i < songLength; i += 4) {
        int indices[4];
        for (int k = 0; k < 4; k++) {
            indices[k] = getBase64Index(song[i + k]);
        }

        decryptedSong[j++] = (indices[0] << 2) | (indices[1] >> 4);
        if (indices[2] < 64) {
            decryptedSong[j++] = (indices[1] << 4) | (indices[2] >> 2);
            if (indices[3] < 64) {
                decryptedSong[j++] = (indices[2] << 6) | indices[3];
            }
        }
    }

    decryptedSong[decryptedSongLength] = '\0';
    // printf("%s\n", song);
    char command[200] = "echo \"";
    strcat(command, decryptedSong);
    strcat(command, "\" >> playlist.txt");
    // printf("%s\n", command);
    system(command);

}

void hex(char * song) {
    int hexLength = strlen(song);

    char decryptedSong[hexLength / 2 + 1];

    for (int i = 0; i < hexLength; i += 2) {
        char hexPair[3] = {
            song[i],
            song[i + 1],
            '\0'
        };

        int hexVal;
        sscanf(hexPair, "%x", & hexVal);

        decryptedSong[i / 2] = (char) hexVal;
    }

    decryptedSong[hexLength / 2] = '\0';

    // printf("%s\n", song);
    char command[200] = "echo \"";
    strcat(command, decryptedSong);
    strcat(command, "\" >> playlist.txt");
    // printf("%s\n", command);
    system(command);

}

void decrypt() {
    FILE * file;
    char buffer[500];
    file = fopen("song-playlist.json", "r");
    if (file == NULL) {
        printf("Failed to open file.\n");
        return;
    }

    int i = 0;
    while (fgets(buffer, sizeof(buffer), file)) {
        if (strstr(buffer, "rot13")) {
            strcpy(method[i], buffer);
            fgets(buffer, sizeof(buffer), file);
            strcpy(song[i], buffer);
            i++;
        } else if (strstr(buffer, "base64")) {
            strcpy(method[i], buffer);
            fgets(buffer, sizeof(buffer), file);
            strcpy(song[i], buffer);
            i++;
        } else if (strstr(buffer, "hex")) {
            strcpy(method[i], buffer);
            fgets(buffer, sizeof(buffer), file);
            strcpy(song[i], buffer);
            i++;
        }

    }

    int numberOfSong = i;

    extractContent(numberOfSong);

    for (int i = 0; i < numberOfSong; i++) {
        if (strcmp(method[i], "rot13") == 13) {
            rot13(song[i]);
        } else if (strcmp(method[i], "base64") == 13) {
            base64_decrypt(song[i]);
        } else if (strcmp(method[i], "hex") == 13) {
            hex(song[i]);
        } else {
            printf("unknown encryption\n");
        }
    }

    fclose(file);

}

void execute(char * command) {
    printf("%s\n", command);
    if (strcmp(command, "DECRYPT") == 0) {
        printf("DECRYPTING...\n");
        decrypt();
        printf("DECRYPTING COMPLETE\n");

    } else {
        printf("Perintah tidak diketahui\n");
    }
}

struct msgBuff {
    long msgType;
    char msgText[MSG_SIZE];
};

int main() {
    key_t msgQueKey;
    int msgId;

    struct msgBuff msg;

    if ((msgQueKey = ftok(".", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((msgId = msgget(msgQueKey, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    while (1) {
        if (msgrcv(msgId, & msg, MSG_SIZE, 1, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        printf("Perintah DIterima: %s\n", msg.msgText);

        if (strcmp(msg.msgText, "EXIT") == 0) {
            break;
        }

        execute(msg.msgText);
    }

}