#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

#define MSG_SIZE 512 

struct msgBuff {
    long msgType;
    char msgText[MSG_SIZE];
};

int main() {
    key_t msgQueKey;
    int msgId;

    struct msgBuff msg;

    if ((msgQueKey = ftok(".", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((msgId = msgget(msgQueKey, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    char command[100];
    printf("Masukkan Perintah:\n");
    while (1) {
        scanf("%s", command);

        msg.msgType = 1;
        strncpy(msg.msgText, command, MSG_SIZE);

        if (msgsnd(msgId, & msg, strlen(msg.msgText) + 1, 0) == -1) {
            perror("msgsnd");
            exit(1);
        }

        if (strcmp(command, "EXIT") == 0) {
            break;
        }

    }

}