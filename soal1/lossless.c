#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#define MAX_CHAR 256

struct Node {
  
    char data;
    unsigned freq;
    struct Node *left;
    struct Node *right;
};

void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {

    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        exit(1);
    }

    int c;
    while ((c = fgetc(file)) != EOF) {
        freq[c]++;
    }

    fclose(file);
}

struct Node* create_node(char data, unsigned freq) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->data = data;
    node->freq = freq;
    return node;
}

struct Node* mergeNode(struct Node* left, struct Node* right) {
    struct Node* node = create_node('\0', left->freq + right->freq);
    node->left = left;
    node->right = right;
    return node;
}

struct Node* create_huffman(unsigned int freq[MAX_CHAR]) {
    struct Node *nodes[MAX_CHAR];
    int i, j;
    for (i = 0, j = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
            nodes[j] = create_node(i, freq[i]);
            j++;
        }
    }
    int size = j;
    while (size > 1) {
        struct Node *left = nodes[size - 2];
        struct Node *right = nodes[size - 1];
        struct Node *merged = mergeNode(left, right);
        nodes[size - 2] = merged;
        size--;
    }

    return nodes[0];
}

void generate_code(struct Node* root, char* code, int depth, char* huffCode[MAX_CHAR]) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        code[depth] = '\0';
        huffCode[root->data] = strdup(code);
    }

    code[depth] = '0';
    generate_code(root->left, code, depth + 1, huffCode);

    code[depth] = '1';
    generate_code(root->right, code, depth + 1, huffCode);

    if (depth > 0) {
        free(huffCode[root->data]);
    }
}

void save_huffman(struct Node* root, FILE* file) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        fputc('1', file);
        fputc(root->data, file);
    } else {
        fputc('0', file);
        save_huffman(root->left, file);
        save_huffman(root->right, file);
    }
}

void compress_file(const char* input_filename, const char* output_filename, char* huffCode[MAX_CHAR]) {
    FILE* input_file = fopen(input_filename, "r");
    if (input_file == NULL) {
        exit(1);
    }
    FILE* output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        exit(1);
    }
    int c;
    while ((c = fgetc(input_file)) != EOF) {
        char* code = huffCode[c];
        fwrite(code, sizeof(char), strlen(code), output_file);

    }
    fclose(input_file);
    fclose(output_file);
}

int main() {
    const char* input_filename = "file.txt";
    const char* compressed_filename = "compressed_file.txt";

    unsigned int freq[MAX_CHAR] = {0};
    count_freq(input_filename, freq);

    struct Node* root = create_huffman(freq);

    int pipe_fd[2];
    if (pipe(pipe_fd) == -1) {
        exit(1);
    }

    pid_t pid = fork();

    if (pid < 0) {
        exit(1);
    } else if (pid > 0) {
        close(pipe_fd[1]);
        unsigned int child_freq[MAX_CHAR];
        read(pipe_fd[0], child_freq, sizeof(child_freq));
        unsigned long long int original_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            original_bits += freq[i] * sizeof(char) * 8;
        }

        char* huffCode[MAX_CHAR] = {NULL};
        char code[MAX_CHAR];
        generate_code(root, code, 0, huffCode);

        FILE* compressed_file = fopen(compressed_filename, "wb");
        if (compressed_file == NULL) {
            exit(1);
        }
        save_huffman(root, compressed_file);
        fclose(compressed_file);
        compress_file(input_filename, compressed_filename, huffCode);

        unsigned long long int compressed_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0 && huffCode[i] != NULL) {
                compressed_bits += freq[i] * strlen(huffCode[i]);
            }
        }

        printf("total original file: %llu\n", original_bits);
        printf("total compressed file: %llu\n", compressed_bits);

        wait(NULL);

    } else {
        close(pipe_fd[0]);
        write(pipe_fd[1], freq, sizeof(freq));
        exit(0);
    }

    return 0;
}
