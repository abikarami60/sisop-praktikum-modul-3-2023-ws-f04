# sisop-praktikum-modul-3-2023-WS-F04
# Praktikum Sistem Operasi Modul 3
# Kelompok F04
- Fathan Abi Karami 5025211156
- Heru Dwi Kurniawan 5025211055
- Alya Putri Salma 5025211174

# Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut!
## Poin A
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

```bash
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {

    // buka file dengan mode r (read only) dan simpan file ke pointer bernama FILE
    FILE *file = fopen(filename, "r");
    //kondisi apakah file berhasil dibuka atau tidak, jika tidak berhasil maka program akan keluar 
    if (file == NULL) {
        exit(1);
    }

    //variabel c dengan tipe data integer
    int c;
    // membaca karakter satu persatu dari file yang dibuka sebelumnya hingga akhir file (EOF) tercapai
    while ((c = fgetc(file)) != EOF) {
        // menambah jumlah frekuensi karakter pada array freq sesuai dengan karakter yang dibaca
        freq[c]++;
    }

    //menutup file yang telah dibuka sebelumnya dengan funsi fclose
    fclose(file);
}
```
`count_freq()`: Fungsi ini membuka file dengan mode "r" (read-only) dan membaca karakter satu per satu dari file tersebut hingga EOF (end-of-file) tercapai. Setiap karakter yang dibaca akan menambah jumlah frekuensi kemunculan karakter pada array freq. Array freq digunakan untuk menyimpan frekuensi kemunculan tiap karakter dalam file yang akan dikompresi dan akan dikirimkan ke child process untuk dihitung lebih lanjut.
## Poin B
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.


Solusi: Pada child process, terima data frekuensi setiap huruf yang dikirimkan oleh parent process, Buat pohon Huffman berdasarkan data frekuensi huruf yang diterima. Lakukan kompresi pada file dengan menggunakan pohon Huffman yang telah dibuat. Simpan file yang telah terkompresi.
```bash
// fungsi untuk menulis data ke dalam file compressed
void write_code(const char* filename, char* huffCode[MAX_CHAR]) {
    // buka file dengan mode r (read only) dan simpan file ke pointer bernama inputFile
    FILE *inputFile = fopen(filename, "r");
    //kondisi apakah file berhasil dibuka atau tidak, jika tidak berhasil maka program akan keluar 
    if (inputFile == NULL) {
        exit(1);
    }

    // buka file dengan mode w (write only) dan simpan file ke pointer bernama outputFile
    FILE *outputFile = fopen("compressed.txt", "w");
    //kondisi apakah file berhasil dibuka atau tidak, jika tidak berhasil maka program akan keluar 
    if (outputFile == NULL) {
        exit(1);
    }

    // variabel c dengan tipe data integer
    int c;
    // loop membaca karakter satu persatu dari file yang dibuka sebelumnya hingga akhir file (EOF) tercapai
    while ((c = fgetc(inputFile)) != EOF) {
        // tulis kode huffman untuk karakter yang dibaca ke dalam file compressed
        fputs(huffCode[c], outputFile);
    }

    //menutup file yang telah dibuka sebelumnya dengan funsi fclose
    fclose(inputFile);
    fclose(outputFile);
}
```
Fungsi yang tertera di atas berguna untuk menulis data ke dalam sebuah file compressed dengan menggunakan algoritma Huffman. Fungsi ini akan membuka file dengan mode "read only" dan membaca satu karakter demi satu dari file tersebut hingga mencapai akhir file. Setelah itu, fungsi akan menuliskan kode Huffman untuk karakter yang dibaca ke dalam file compressed. Selain itu, fungsi ini akan menutup file yang telah dibuka sebelumnya dengan menggunakan fungsi fclose.
## Poin C 
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
```bash
// pada child process, simpan Huffman tree pada file terkompresi
// untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman
// dan kirimkan kode tersebut ke program dekompresi menggunakan pipe

if (pid == 0) { //child process
    //membuat file dengan nama file compressed
    int fd = open("compressed", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    // menuliskan frekuensi karakter pada file compressed
    write(fd, freq, MAX_CHAR * sizeof(unsigned int));

    //membuat huffman tree
    struct Node *root = create_huffman(freq);
    //menginisialisasi array huffCode dengan karakter null
    char* huffCode[MAX_CHAR];
    for (int i = 0; i < MAX_CHAR; i++) {
        huffCode[i] = NULL;
    }

    //menggenerate kode huffman
    char code[MAX_CHAR];
    code[0] = '\0';
    generate_code(root, code, 0, huffCode);

    //proses membaca file dan mengkompresi teks
    char buffer[BLOCK];
    int nread;
    while ((nread = read(fd_input, buffer, BLOCK)) > 0) {
        for (int i = 0; i < nread; i++) {
            //menuliskan kode huffman untuk karakter ke file compressed
            char* code = huffCode[buffer[i]];
            int len = strlen(code);
            for (int j = 0; j < len; j++) {
                write(fd, &code[j], 1);
            }
        }
    }
    //menutup file compressed
    close(fd);
    exit(0);
}
```
Pada potongan kode di atas, kita dapat melihat bagaimana di child process, Huffman tree dibuat dan disimpan pada file terkompresi. Setelah itu, untuk setiap karakter pada file yang akan dikompresi, karakter tersebut diubah menjadi kode Huffman dan dikirimkan ke program dekompresi menggunakan pipe.

## Poin D 
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
```bash
int pipe_fd[2];

// membuat pipe
if (pipe(pipefd) == -1) {
    perror("pipe");
    exit(1);
}

// fork process
pid_t pid = fork();

// jika pid == 0, maka ini adalah child process
if (pid == 0) {
    // tutup bagian tulis dari pipe
    close(pipefd[1]);

    // redirect input dari pipe
    dup2(pipefd[0], STDIN_FILENO);

    // jalankan program dekompresi
    char *args[] = {"./decompress", NULL};
    execvp(args[0], args);

    // keluar jika gagal
    perror("execvp");
    exit(1);
} else {
    // tutup bagian baca dari pipe
    close(pipefd[0]);

    // tulis hasil kompresi ke pipe
    write(pipefd[1], compressed_data, compressed_size);

    // tutup bagian tulis dari pipe
    close(pipefd[1]);

    // tunggu child process selesai
    wait(NULL);

    // baca Huffman tree dari file terkompresi
    FILE *in = fopen(argv[1], "rb");
    struct Node *root = read_tree(in);

    // baca kode Huffman dari pipe
    char *huffman_code[MAX_CHAR];
    read(pipefd[0], huffman_code, sizeof(huffman_code));

    // lakukan dekompresi
    decompress(huffman_code, compressed_size, root);
}
```
Potongan kode di atas melakukan pembuatan pipe untuk mengirimkan hasil kompresi dari child process ke parent process. Hasil kompresi kemudian ditulis ke sisi penulisan (tulis) dari pipe. Di parent process, Huffman tree dibaca dari file terkompresi menggunakan fungsi read_tree() dan hasilnya disimpan di dalam variabel root. Selanjutnya, kode Huffman dibaca dari sisi pembacaan (baca) dari pipe dan disimpan di dalam array huffman_code. Akhirnya, dilakukan proses dekompresi menggunakan fungsi decompress().

## Poin E
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
```bash
// proses parent
if (pid > 0) {
    // tunggu proses child selesai
    wait(NULL);
    // hitung ukuran file sebelum dikompresi
    struct stat st;
    stat(argv[1], &st);
    double size_before = st.st_size * 8;
    // hitung ukuran file setelah dikompresi
    double size_after = count_bits(huffCode, freq) * 1.0;
    // tampilkan perbandingan ukuran file sebelum dan sesudah dikompresi
    printf("Ukuran file sebelum kompresi: %.2lf bit\n", size_before);
    printf("Ukuran file setelah kompresi: %.2lf bit\n", size_after);
    printf("Rasio kompresi: %.2lf%%\n", (size_before - size_after) / size_before * 100);
}

```
program menunggu proses child selesai dengan menggunakan `wait(NULL)` untuk memastikan bahwa proses child telah selesai sebelum dilanjutkan. Setelah itu, program menghitung ukuran file sebelum dan setelah dikompresi dengan menggunakan fungsi `count_bits()` yang menghitung jumlah bit setelah kompresi dengan algoritma Huffman. Kemudian, program menampilkan perbandingan ukuran file sebelum dan setelah dikompresi, serta rasio kompresi dalam persen.

# Soal 2 
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.
## Poin A 
Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.
#include <stdio.h>

//mohon maaf mas untuk no 2 belum selesai mas dan masih belum bisa jalan untuk programnya

int main(){

 int matriks_1[4][2];matriks_2[2][2];
 int a, b, c, d, p, q, r;
  printf("jumlah baris matriks_1: ");

  printf("jumlah kolom matriks_1 : ");

  printf("jumlah baris matriks_2: ");
  
  printf("jumlah kolom matriks_2 : ");
}




# Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.
## Poin A
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

stream.c:

Message queue stream.c, untuk menerima perintah dari user.c

```c
// stream.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

#define MSG_SIZE 512 // maximum length of the message that can be sent allowed

struct msgBuff {
    long msgType;
    char msgText[MSG_SIZE];
};

int main() {
    key_t msgQueKey;
    int msgId;

    struct msgBuff msg;

    if ((msgQueKey = ftok(".", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((msgId = msgget(msgQueKey, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    while (1) {
        if (msgrcv(msgId, & msg, MSG_SIZE, 1, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

    }

}
```
user.c:

Message queue user.c digunakan untuk mengirim perintah ke stream.c

```c
// user.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

#define MSG_SIZE 512 

struct msgBuff {
    long msgType;
    char msgText[MSG_SIZE];
};

int main() {
    key_t msgQueKey;
    int msgId;

    struct msgBuff msg;

    if ((msgQueKey = ftok(".", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((msgId = msgget(msgQueKey, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    char command[100];
    printf("Masukkan Perintah:\n");
    while (1) {
        scanf("%s", command);

        msg.msgType = 1;
        strncpy(msg.msgText, command, MSG_SIZE);

        if (msgsnd(msgId, & msg, strlen(msg.msgText) + 1, 0) == -1) {
            perror("msgsnd");
            exit(1);
        }

    }

}
```

## Poin B
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:

stream.c:

proses perintah dari user.c, jika DECRYPT maka lakukan langkah berikut:

1. buka file json
2. dapatkan metode dekripsi beserta lagunya
3. lakukan extract isi metode dan lagu
4. dekripsi lagu sesuai dengan metode dekripsi
5. masukkan ke file playlist.txt
```c
char method[1000][100];
char song[1000][1000];

void extractContent(int n) {
    for (int i = 0; i < n; i++) {
        char * methodSubstr = strstr(method[i], ": ");
        if (methodSubstr != NULL) {
            methodSubstr += 2;
        }

        char * commaPtr = strchr(methodSubstr, ',');
        if (commaPtr != NULL) {
            strcpy(commaPtr, commaPtr + 1);
        }

        char * quotePtr = strchr(methodSubstr, '\"');
        while (quotePtr != NULL) {
            strcpy(quotePtr, quotePtr + 1);
            quotePtr = strchr(methodSubstr, '\"');
        }

        // printf("%d %s ", i, methodSubstr);
        strcpy(method[i], methodSubstr);

        char * songSubstr = strstr(song[i], ": ");
        if (songSubstr != NULL) {
            songSubstr += 2;
        }

        int len = strlen(songSubstr);
        for (int i = 0; i < len; i++) {
            songSubstr[i] = songSubstr[i + 1];
        }

        songSubstr[len - 4] = '\0';

        snprintf(song[i], 500, "%s", songSubstr);
        //strcpy(song[i],
    }

}

void rot13(char * song) {
    char * p = song;
    while ( * p != '\0') {
        if ( * p >= 'a' && * p <= 'z') {
            * p = ( * p - 'a' + 13) % 26 + 'a';
        } else if ( * p >= 'A' && * p <= 'Z') {
            * p = ( * p - 'A' + 13) % 26 + 'A';
        }
        p++;
    }

    // printf("%s\n", song);
    char command[100] = "echo \"";
    strcat(command, song);
    strcat(command, "\" >> playlist.txt");
    // printf("%s\n", command);
    system(command);

}

int getBase64Index(char c) {
    if (c >= 'A' && c <= 'Z')
        return c - 'A';
    if (c >= 'a' && c <= 'z')
        return c - 'a' + 26;
    if (c >= '0' && c <= '9')
        return c - '0' + 52;
    if (c == '+')
        return 62;
    if (c == '/')
        return 63;
    return -1;
}

void base64_decrypt(const char * song) {
    size_t songLength = strlen(song);
    size_t decryptedSongLength = (songLength / 4) * 3;
    if (song[songLength - 1] == '=')
        decryptedSongLength--;
    if (song[songLength - 2] == '=')
        decryptedSongLength--;

    char * decryptedSong = (char * ) malloc(decryptedSongLength + 1);
    if (decryptedSong == NULL) {
        printf("Failled to ALlocate Memory\n");
        return;
    }

    size_t i, j;
    for (i = 0, j = 0; i < songLength; i += 4) {
        int indices[4];
        for (int k = 0; k < 4; k++) {
            indices[k] = getBase64Index(song[i + k]);
        }

        decryptedSong[j++] = (indices[0] << 2) | (indices[1] >> 4);
        if (indices[2] < 64) {
            decryptedSong[j++] = (indices[1] << 4) | (indices[2] >> 2);
            if (indices[3] < 64) {
                decryptedSong[j++] = (indices[2] << 6) | indices[3];
            }
        }
    }

    decryptedSong[decryptedSongLength] = '\0';
    // printf("%s\n", song);
    char command[200] = "echo \"";
    strcat(command, decryptedSong);
    strcat(command, "\" >> playlist.txt");
    // printf("%s\n", command);
    system(command);

}

void hex(char * song) {
    int hexLength = strlen(song);

    char decryptedSong[hexLength / 2 + 1];

    for (int i = 0; i < hexLength; i += 2) {
        char hexPair[3] = {
            song[i],
            song[i + 1],
            '\0'
        };

        int hexVal;
        sscanf(hexPair, "%x", & hexVal);

        decryptedSong[i / 2] = (char) hexVal;
    }

    decryptedSong[hexLength / 2] = '\0';

    // printf("%s\n", song);
    char command[200] = "echo \"";
    strcat(command, decryptedSong);
    strcat(command, "\" >> playlist.txt");
    // printf("%s\n", command);
    system(command);

}

void decrypt() {
    FILE * file;
    char buffer[500];
    file = fopen("song-playlist.json", "r");
    if (file == NULL) {
        printf("Failed to open file.\n");
        return;
    }

    int i = 0;
    while (fgets(buffer, sizeof(buffer), file)) {
        if (strstr(buffer, "rot13")) {
            strcpy(method[i], buffer);
            fgets(buffer, sizeof(buffer), file);
            strcpy(song[i], buffer);
            i++;
        } else if (strstr(buffer, "base64")) {
            strcpy(method[i], buffer);
            fgets(buffer, sizeof(buffer), file);
            strcpy(song[i], buffer);
            i++;
        } else if (strstr(buffer, "hex")) {
            strcpy(method[i], buffer);
            fgets(buffer, sizeof(buffer), file);
            strcpy(song[i], buffer);
            i++;
        }

    }

    int numberOfSong = i;

    extractContent(numberOfSong);

    for (int i = 0; i < numberOfSong; i++) {
        if (strcmp(method[i], "rot13") == 13) {
            rot13(song[i]);
        } else if (strcmp(method[i], "base64") == 13) {
            base64_decrypt(song[i]);
        } else if (strcmp(method[i], "hex") == 13) {
            hex(song[i]);
        } else {
            printf("unknown encryption\n");
        }
    }

    fclose(file);

}

void execute(char * command) {
    printf("%s\n", command);
    if (strcmp(command, "DECRYPT") == 0) {
        printf("DECRYPTING...\n");
        decrypt();
        printf("DECRYPTING COMPLETE\n");

    } else {
        printf("Perintah tidak diketahui\n");
    }
}

int main() {
    key_t msgQueKey;
    int msgId;

    struct msgBuff msg;

    if ((msgQueKey = ftok(".", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((msgId = msgget(msgQueKey, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    while (1) {
        if (msgrcv(msgId, & msg, MSG_SIZE, 1, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        printf("Perintah DIterima: %s\n", msg.msgText);

        if (strcmp(msg.msgText, "EXIT") == 0) {
            break;
        }

        execute(msg.msgText);
    }

}
```

output:
![](./img/soal3/ss%20terminal.png)
![](./img/soal3/ss%20playlist%201.png)
![](./img/soal3/ss%20playlist%202.png)

Kendala dan error:
1. kendala dalam melakukan parsing file json

# Soal 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 
## Poin A
Download dan unzip file tersebut dalam kode c bernama unzip.c.

download zip file:

buat process baru untuk download file, gunakan system() untuk mengeksekusi bash command untuk download zip file
```c
int downloadStatus;
    pid_t downloadPID = fork();

    if (downloadPID < 0) {
        exit(EXIT_FAILURE);
    }

    if (downloadPID == 0) {
        char * cmd_download = "wget \"https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download\" -O hehe.zip NULL";
        system(cmd_download);
    }

    waitpid(downloadPID, & downloadStatus, 0);
```

unzip:

buat process baru, gunakan system() untuk mengeksekusi bash script untuk melakukan unzip file
```c
int unzipStat;
    pid_t unzipPid = fork();

    if (unzipPid == 0) {
        char * argv[] = {
            "unzip",
            "-q",
            "hehe.zip",
            NULL
        };
        execv("/usr/bin/unzip", argv);
    }
    waitpid(unzipPid, & unzipStat, 0);
```



source code:
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


int main() {

    int downloadStatus;
    pid_t downloadPID = fork();

    if (downloadPID < 0) {
        exit(EXIT_FAILURE);
    }

    if (downloadPID == 0) {
        char * cmd_download = "wget \"https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download\" -O hehe.zip NULL";
        system(cmd_download);
    }

    waitpid(downloadPID, & downloadStatus, 0);

    int unzipStat;
    pid_t unzipPid = fork();

    if (unzipPid == 0) {
        char * argv[] = {
            "unzip",
            "-q",
            "hehe.zip",
            NULL
        };
        execv("/usr/bin/unzip", argv);
    }
    waitpid(unzipPid, & unzipStat, 0);

    return 0;
}
```
Output: 
Sebelum:
![](./img/soal4/ss%20unzip%20sebelum.png)
Sesudah:
![](./img/soal4/ss%20unzip%20sesudah.png)
## Poin B 
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

Pengerjaan:

buat dan inisisasi array untuk menyimpan jumlah folder dengan extension tertentu dan jumlah file tiap extension:

```c
char extensions[100][100];   // list extension
int extensionsFolder[100];   // jumlah folder dengan extension tertentu
int extensionsCount[100];   // jumlah file tiap extension
int totalExtensions = 0;       // jumlah extension pada file extensions.txt
int maxFiles;                      // max file tiap folder dari max.txt

int main() {

    ///   Initialize Array   ///

    for (int i = 0; i < 100; i++) {
        extensionsFolder[i] = 1;
        extensionsCount[i] = 0;
    }
}
```

dapatkan list extension dari extensions.txt:
```c
int main() {

    ///  Get Extensions from extensions.txt ///

    FILE * fileExtensions = fopen("extensions.txt", "r");

    char buffer[100];

    while (fgets(buffer, 100, fileExtensions) != NULL) {
        int len = strcspn(buffer, "\r\n"); // find position of newline or carriage return character
        buffer[len] = '\0';
        strcpy(extensions[totalExtensions], buffer);
        totalExtensions++;
    }

    fclose(fileExtensions);
}
```

sort extension:
```c
void sortExtensions(char arr[][100], int n) {
    char temp[100];
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (strcmp(arr[j], arr[j + 1]) > 0) {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

int main(){
    
    /// Sort Extension in Ascending order    ///

    sortExtensions(extensions, totalExtensions);
}
```

tambah extension other untuk file extension selain extension pada list:
```c
int main(){
     /// Add other extensions    ///

    strcpy(extensions[totalExtensions], "other");
    totalExtensions++;
}
```

dapatkan max file tiap folder dari max.txt:
```c
int main(){
    /// Get max files number of files from max.txt ///

    FILE * fileMax = fopen("max.txt", "r");

    fgets(buffer, 100, fileMax);
    maxFiles = atoi(buffer);

    fclose(fileMax);
}
```

Buat folder categorized, dan subfolder extensions di dalam folder categorized:

```c
void createCategorizedDirectory() {
    system("mkdir categorized");
    logMade("categorized");

    system("cd categorized && mkdir other");
    logMade("other");

    for (int i = 0; i < totalExtensions - 1; i++) {
        char command[255];
        sprintf(command, "mkdir -p categorized/%s", extensions[i]);
        system(command);
        logMade(extensions[i]);
    }
}

int main(){
    ///   Create Categorized Dircetory   ///

    createCategorizedDirectory();
}
```

copy file ke sub folder berdasarkan extensinya:
1. buka direktori yang akan dikategorikan
2. iterasi tiap entry yang ada pada direktori
3. jika entri merupakan direktori terendiri, maka lakukan rekursi dengan argumen path direktori tersebut
4. jika merupakan file pindahkan sesuai extensinya ke sub folder yang ada di categorized
    1. dapatkan extensionnya, dan ubah menajadi lowercase
    2. jika tidak memiliki extension, biarkan kosong
    3. jika extension ada pada list extension maka pidahkan ke sub folder yang sesuai
        1. dapatkan jumlah file pada folder tujuan copy
        2. jika kurang dari 10 oindahkan ke folder tersebut
        3. jika tidak buat folder baru lalu pindahkan ke fodler tersebut
    4. jika tidak ada pada list pindahkan ke folder other
5. tutup direktori
```c
// cek apakah extension file ada pada list extension
int isFound(char extension[]) {
    int isFound = 0;
    for (int i = 0; i < totalExtensions; i++) {
        if (strcmp(extension, extensions[i]) == 0) {
            isFound = 1;
        }
    }

    return isFound;
}

// membuat extension menjadi lowercase
void toLowercase(char * str) {
    int i;
    for (i = 0; str[i] != '\0'; i++) {
        str[i] = tolower(str[i]);
    }
}

// untuk menambahkan quotation mark pada path file agar bisa dieksekusi perintah cp pada system()
char * encloseStringWithQuotes(char * str) {
    size_t len = strlen(str);
    char * newStr = malloc(len + 3);
    if (newStr == NULL) {
        fprintf(stderr, "Error: failed to allocate memory\n");
        return NULL;
    }
    newStr[0] = '\'';
    strcpy( & newStr[1], str);
    newStr[len + 1] = '\'';
    newStr[len + 2] = '\0';
    return newStr;
}

// dapat jumlah file yang ada pada folder
int count_files(const char * path) {
    DIR * dir;
    struct dirent * entry;
    int count = 0;

    if ((dir = opendir(path)) == NULL) {
        perror("opendir() error");
        return -1;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry -> d_type == DT_REG) {
            count++;
        }
    }

    closedir(dir);
    return count;
}

// dapatkan index array extensi
int getExtIndex(char * ext) {
    for (int i = 0; i < totalExtensions; i++) {
        if (strcmp(ext, extensions[i]) == 0) {
            return i;
        }
    }

    return -1;
}

// routine prototype
void * categorized_thread(void * arg);

// categorize function
void categorized(const char * path) {
    DIR * directory = opendir(path);

    logAccessed(path);

    if (directory == NULL) {
        printf("Could not open directory: %s\n", path);
        return;
    }

    struct dirent * entry;
    while ((entry = readdir(directory)) != NULL) {
        if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
            continue;
        }

        char fullPath[1024];
        snprintf(fullPath, sizeof(fullPath), "%s/%s", path, entry -> d_name);

        if (entry -> d_type == DT_DIR) {
            // printf("Directory: %s\n", fullPath);
            pthread_t tid;
            pthread_create( & tid, NULL, categorized_thread, fullPath);
            pthread_join(tid, NULL);
        } else {
            char * ext = strrchr(entry -> d_name, '.');
            if (ext) {
                ext = ext + 1;
                toLowercase(ext);
            } else ext = "";
            // printf("%s ", fullPath);

            char * newFullPath = encloseStringWithQuotes(fullPath);

            if (isFound(ext)) {
                int extIndex = getExtIndex(ext);
                extensionsCount[extIndex]++;
                // printf("%s %d\n", ext, extIndex);

                if (extensionsFolder[extIndex] == 1) {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

                    int n = count_files(dest);

                    if (n < maxFiles - 1) {
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);
                        logMoved(ext, fullPath, ext);
                    } else {
                        extensionsFolder[extIndex]++;

                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

                        char num[10];
                        sprintf(num, "%d", extensionsFolder[extIndex]);
                        strcat(cmd_mkdir, num);

                        system(cmd_mkdir);
                        // printf("%s\n", cmd_mkdir);
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        logMade(folderName);

                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    }
                } else {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

                    char num[10];
                    sprintf(num, "%d", extensionsFolder[extIndex]);
                    strcat(dest, num);

                    int n = count_files(dest);

                    if (n < maxFiles - 1) {
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    } else {
                        extensionsFolder[extIndex]++;

                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

                        char num[10];
                        sprintf(num, "%d", extensionsFolder[extIndex]);
                        strcat(cmd_mkdir, num);

                        // printf("%s\n", cmd_mkdir);
                        system(cmd_mkdir);
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        logMade(folderName);

                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);
                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    }
                }
            } else {
                extensionsCount[totalExtensions - 1]++;
                char command[100] = "cp ";
                strcat(command, newFullPath);
                strcat(command, " \"./categorized/other\"");

                // printf("%s\n", command);
                system(command);
                logMoved(ext, fullPath, "other");
            }
        }
    }

    closedir(directory);
}

// routine
void * categorized_thread(void * arg) {
    const char * path = (const char * ) arg;
    categorized(path);
    pthread_exit(NULL);
}

int main(){
    ///   Categorize files   ///

    pthread_t tid;
    pthread_create( & tid, NULL, categorized_thread, "./files");
    pthread_join(tid, NULL);
}
```

## Poin C
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file

extension_b : banyak_file

extension_c : banyak_file

other : banyak_file

```c
int main(){
    for (int i = 0; i < totalExtensions; i++) {
        printf("%s: %d\n", extensions[i], extensionsCount[i]);
    }
}
```

## Poin D
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

```c
// routine prototype
void * categorized_thread(void * arg);

void categorized(const char * path) {
    DIR * directory = opendir(path);

    logAccessed(path);

    if (directory == NULL) {
        printf("Could not open directory: %s\n", path);
        return;
    }

    struct dirent * entry;
    while ((entry = readdir(directory)) != NULL) {
        if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
            continue;
        }

        char fullPath[1024];
        snprintf(fullPath, sizeof(fullPath), "%s/%s", path, entry -> d_name);

        if (entry -> d_type == DT_DIR) {
            // buat thread baru ketika melakukan rekursi saat membuka tiap sub folder
        
            pthread_t tid;
            pthread_create( & tid, NULL, categorized_thread, fullPath);
            pthread_join(tid, NULL);
        } else {
            // rest of code
        }
    }

    closedir(directory);
}

// routine
void * categorized_thread(void * arg) {
    const char * path = (const char * ) arg;
    categorized(path);
    pthread_exit(NULL);
}


int main(){
    ///   Categorize files   ///

    // buat thread baru ketika mengakses folder pertama kali pada, yaitu folder files
    pthread_t tid;
    pthread_create( & tid, NULL, categorized_thread, "./files");
    pthread_join(tid, NULL);
}
```

## Poin E
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.

DD-MM-YYYY HH:MM:SS ACCESSED [folder path]

DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]

DD-MM-YYYY HH:MM:SS MADE [folder name]

examples : 

02-05-2023 10:01:02 ACCESSED files

02-05-2023 10:01:03 ACCESSED files/abcd

02-05-2023 10:01:04 MADE categorized

02-05-2023 10:01:05 MADE categorized/jpg

02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg

Catatan:
Path dimulai dari folder files atau categorized

Simpan di dalam log.txt

ACCESSED merupakan folder files beserta dalamnya

Urutan log tidak harus sama:

log dibuat setiap perintah system()

```c
// dapatkan current time
void getCurrentTime(char * timeString) {
    time_t curTime;
    struct tm * timeInfo;

    curTime = time(NULL);

    timeInfo = localtime( & curTime);
    strftime(timeString, 20, "%d-%m-%y %H:%M:%S", timeInfo);
    // printf("%s\n", timeString);

}

// log untuk tiap accessed folder dan sub folder
void logAccessed(const char * path) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[100];
    logCommand[0] = '\0';
    
    strcat(logCommand, timeString);
    strcat(logCommand, " ACCESSED ");
    strcat(logCommand, path);

    char command[200];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);

}

// log saat memindahkan file
void logMoved(char * ext, char * source, char * dest) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[200];
    logCommand[0] = '\0';

    strcat(logCommand, timeString);
    strcat(logCommand, " MOVED ");
    strcat(logCommand, ext);
    strcat(logCommand, " file : ");
    strcat(logCommand, source);
    strcat(logCommand, " > ");
    strcat(logCommand, dest);

    char command[100];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);

}

// log saat membuat folder
void logMade(char * name) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[100];
    logCommand[0] = '\0';

    strcat(logCommand, timeString);
    strcat(logCommand, " MADE ");
    strcat(logCommand, name);

    char command[100];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);
    
}

void createCategorizedDirectory() {
    system("mkdir categorized");
    logMade("categorized");     // log made

    system("cd categorized && mkdir other");
    logMade("other");           // log made

    for (int i = 0; i < totalExtensions - 1; i++) {
        char command[255];
        sprintf(command, "mkdir -p categorized/%s", extensions[i]);
        system(command);
        logMade(extensions[i]);   // log made
    }
}

void categorized(const char * path) {
    DIR * directory = opendir(path);

    logAccessed(path);   // log accesed

    // code 

    if (n < maxFiles - 1) {
        // code

        // printf("%s\n", command);
        system(command);
        logMoved(ext, fullPath, ext);  // log moved
    } else {

        // code

        system(cmd_mkdir);
        // printf("%s\n", cmd_mkdir);
        char folderName[20];
        folderName[0] = '\0';
        strcat(folderName, ext);
        strcat(folderName, num);
        logMade(folderName);  // log made

        // code

        // printf("%s\n", command);
        system(command);

        char temp[100];
        strcpy(temp, dest + 14);

        logMoved(ext, fullPath, temp);  // log moved
    }

    system(command);
    logMoved(ext, fullPath, "other");  // log moved
}
```
Output:

sebelum: 
![](./img/soal4/ss%20categorize%20sebelum.png)
Sesudah:
![](./img/soal4/ss%20categrize%20sesudah.png)
![](./img/soal4/ss%20isi%20folder%20categorized.png)
![](./img/soal4/ss%20emc.png)
![](./img/soal4/ss%20emc2.png)
![](./img/soal4/ss%20emc7.png)
![](./img/soal4/ss%20txt.png)
![](./img/soal4/ss%20txt8.png)
![](./img/soal4/ss%20other.png)
![](./img/soal4/ss%20log.txt.png)

## poin F
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
1. Untuk menghitung banyaknya ACCESSED yang dilakukan.

2. Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder 
tersebut, terurut secara ascending.

3. Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

Pengerjaan:

inisasi Array
```c
char folders[100][500];  // list folder
int fileCount[100];      // list jumlah file tiap folder
int folderCount = 0;     // jumlah folder

char extensions[100][500];  // list extension
int extensionCount[100];    // list jumlah file tiap extension
int totalExtension = 0;     // total extension

int main(){
    ///   Insisasi   ///
    for (int i = 0; i < 100; i++) {
        fileCount[i] = 0;
    }

    for (int i = 0; i < 100; i++) {
        extensionCount[i] = 0;
    }

}
```

buka file:
```c
int main(){
    ///   Buka File   ///	

    FILE * fp = fopen("log.txt", "r");

    if (fp == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }
    char buffer[200];  // buffer untuk menyimpan baris log
}
```
hitung jumlah accessed
```c
int main(){
    ///   Hitung Jumlah ACCESSED   ///
    int AccessedCount = 0;
    while (fgets(buffer, 200, fp)) {
        if (strstr(buffer, "ACCESSED") != NULL) {
            AccessedCount++;
        }
    }

    printf("Jumlah 'ACCESSED': %d\n", AccessedCount);
}
```

list dan sort folder:
```c
void sort(char arr[][500], int n) {
    int i, j;
    char temp[100];
    for (i = 0; i < n - 1; i++) {
        for (j = 0; j < n - i - 1; j++) {
            if (strcmp(arr[j], arr[j + 1]) > 0) {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

int main(){
    ///   List Folder   ///

    fp = fopen("log.txt", "r");
    char * folder;

    while (fgets(buffer, 200, fp)) {
        folder = strstr(buffer, "MADE ");

        if (folder != NULL) {
            folder += strlen("MADE ");
            folder[strlen(folder) - 2] = '\0';
            strcpy(folders[folderCount], folder);
            folderCount++;

        }
    }

    ///   Sort Folder   ///

    sort(folders, folderCount);
}
```

hitung jumlah file tiap folder:
```c
int getFolderIndex(char * folder) {
    for (int i = 0; i < folderCount; i++) {
        if (strcmp(folder, folders[i]) == 0) {
            return i;
        }
    }

    return -1;
}

int main(){
    ///   Hitung Jumlah File Tiap Folder   ///

    fp = fopen("log.txt", "r");
    char temp[100];

    while (fgets(buffer, 200, fp)) {
        if (folder = strstr(buffer, "> ")) {
            folder[strlen(folder) - 2] = '\0';
            strcpy(temp, folder + 2);

            int folderIndex = getFolderIndex(temp);

            fileCount[getFolderIndex(temp)]++;

        }

    }

}
```

list dan sort extension:
```c
int main(){
    ///   List Extension   ///

    char * extension;
    fp = fopen("log.txt", "r");
    while (fgets(buffer, 200, fp)) {
        if (extension = strstr(buffer, "MOVED ")) {
            strcpy(temp, extension + 6);
            strcpy(extension, temp);

            if (extension[0] == ' ') {
                strcpy(extension, " ");
            } else {
                char * temp2 = strtok(extension, " ");
                strcpy(extension, temp2);

            }

            int extensionExist = 0;
            for (int i = 0; i < totalExtension; i++) {
                if (strcmp(extension, extensions[i]) == 0) {
                    extensionExist = 1;
                }
            }

            if (extensionExist == 0) {

                strcpy(extensions[totalExtension], extension);
                totalExtension++;

            }

        }
    }

    ///   Sort Extension   ///

    sort(extensions, totalExtension);
}
```

hitung jumlah file tiap extension:
```c
int getExtensionIndex(char * extension) {
    for (int i = 0; i < totalExtension; i++) {
        if (strcmp(extension, extensions[i]) == 0) {
            return i;
        }

    }
    return -1;

}

int main(){
    ///   Hitung Jumlah File Tiap Extension   ///

    fp = fopen("log.txt", "r");
    while ((fgets(buffer, 200, fp))) {
        if (extension = strstr(buffer, "MOVED ")) {
            strcpy(temp, extension + 6);
            strcpy(extension, temp);

            if (extension[0] == ' ') {
                strcpy(extension, " ");
            } else {
                char * temp2 = strtok(extension, " ");
                strcpy(extension, temp2);

            }

            int extensionIndex = getExtensionIndex(extension);

            extensionCount[extensionIndex]++;

        }
    }
}
```

print hasilnya:
```c
int main(){
    ///   Print folder dan extension beserta jumlahnya  ///

    printf("---   list folder:   ---\n");

    for (int i = 1; i < folderCount; i++) {

        printf("%s: %d\n", folders[i], fileCount[i]);
    }

    printf("---   list extension:   ---\n");

    for (int i = 0; i < totalExtension; i++) {

        printf("%s: %d\n", extensions[i], extensionCount[i]);
    }
}
```

Output:
![](./img/soal4/ss%20logchecker%201.png)
![](./img/soal4/ss%20logchecker%202.png)

Kendala dan error:
1. Kendala dalam menentukan apakah extension ada dalam list
2. kendala dalam pemindahan pada kasus file pada folder lebih dari max
3. kendala dalam menentukan jumlah extension dari log.txt
